<?php

namespace Gloo\SeptaConnector\Helpers;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class ProductCollection
{

	private $collection = null;
	private $productCollection;

	public function __construct(
		CollectionFactory $productCollection
	){
		$this->productCollection = $productCollection;
	}

	public function getSeptaProductCsvColumnHeader(){
		$headers = [
				   'SKU',
				   'Barcode',
				   'Name',
				   'Common_name',
				   'Is_Variant',
				   'Base_product_name',
				   'Vendor_name',
				   'Has_variants',
				   'Collection Label',
				   'Collection Quantity',
				   'Unit of Measurement Type',
				   'Option 1 Label',
				   'Option 2 Label',
				   'Option 3 Label',
				   'Option 1 Value',
				   'Option 2 Value',
				   'Option 3 Value',
				   'Categories',
				   'Brand',
				   'Reorder_level',
				   'Initial_quantity',
				   'Initial_cost_price',
				   'Initial_selling_price',
				   'Sellable','Purchasable',
				   'Shelf_life'
				];
		return $headers;
	}

	public function all(){
		$this->collection = $this->productCollection->create();
		$this->collection->addAttributeToSelect('*');
		$this->collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
		return $this;
	}

	public function addInventoryCatalog(){
		if(empty($this->collection)){
			throw new \Exception('Cannot add catalog to an empty collection');
		}
		$this->collection->joinField('qty','cataloginventory_stock_item','qty','product_id=entity_id');

		return $this;
	}

	public function get(){
		return $this->collection;
	}

}