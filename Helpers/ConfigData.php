<?php

namespace Gloo\SeptaConnector\Helpers;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class ConfigData extends AbstractHelper
{

	const XML_PATH_SEPTA = 'septa_section/';

	public function getValue($field, $storeId = null)
	{
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getGeneralConfig($code, $storeId = null)
	{

		return $this->getValue(self::XML_PATH_SEPTA .'general/'. $code, $storeId);
	}
}