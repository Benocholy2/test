# Septa Magento Connector

This is a magento extension that interface magento and septa, it connect the following

- Sync products
- Sync sales 
- Sync stock correction
- Sync inventory
- Export products

## Technicality and required knowledge
- install extension using composer install 
- install plugin to magento using **php bin/magento module:enable Septa_Magento_Connector**
- detect extension **php bin/magento setup:upgrade**
- flush cache **php bin/magento cache:flush**
- compile di **php bin/magento setup:di:compile**

## Technologies
- php (magento 2)