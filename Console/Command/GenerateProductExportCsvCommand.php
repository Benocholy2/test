<?php
   namespace Gloo\SeptaConnector\Console\Command;

    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Helper\ProgressBar;
    use Magento\Framework\App\Filesystem\DirectoryList;
    use Magento\Framework\Filesystem;
    use Magento\CatalogInventory\Api\StockStateInterface;
    use Magento\Framework\App\State;
    use Magento\Framework\Exception\LocalizedException;
    use Gloo\SeptaConnector\Helpers\ConfigData;
    use Gloo\SeptaConnector\Helpers\ProductCollection;
    use Gloo\SeptaConnector\Magento\Mail\Template\TransportBuilder;
    use Symfony\Component\Console\Input\InputOption;
    
    /**
     * Class GenerateProductExportCsvCommand
     */
    class GenerateProductExportCsvCommand extends Command
    {
        private $directory, $fileSystem, $transportBuilder, $dataHelper, $state, $productCollectionRepo;
        const Email = 'email';

        public function __construct(
            Filesystem $fileSystem,
            StockStateInterface $stockState,
            State $state,
            ConfigData $dataHelper,
            TransportBuilder $transportBuilder,
            ProductCollection $productCollectionRepo
        )
        {

           $this->fileSystem = $fileSystem;
           $this->stockState = $stockState;
           $this->state = $state;
           $this->dataHelper = $dataHelper;
           $this->transportBuilder = $transportBuilder;
           $this->productCollectionRepo = $productCollectionRepo;

           try{
                $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
            }catch(LocalizedException $e){}

            parent::__construct();
        }
    

        /**
         * @inheritDoc
         */
        protected function configure()
        {
            $options = [new InputOption(self::Email, null, InputOption::VALUE_OPTIONAL, 'email')];

            $this->setName('export:product:csv')
                ->setDescription('export products in a csv format compatible with septa')
                ->setDefinition($options);
        }


        /**
         * Execute the command
         *
         * @param InputInterface $input
         * @param OutputInterface $output
         *
         * @return null|int
         */
        protected function execute( InputInterface $input, OutputInterface $output)
        {
            try{
                $output->writeln('<info>Generating product upload csv for septa</info>');

                $inputEmail = $input->getOption(self::Email);

                if(!empty($inputEmail)){
                    if(!filter_var($inputEmail, FILTER_VALIDATE_EMAIL)){
                        $output->writeln('<error>Invalid email, aborting ...</error>');
                        return;
                    }
                };

                $recipeintEmail = empty($inputEmail) ? $this->dataHelper->getGeneralConfig('recipient_email') : $input->getOption(self::Email);

                if(empty($recipeintEmail)){
                    $output->writeln('<error>Default email not set, kindly set one on the septa configuration settings or you can use the email flag</error>');
                    return;
                }

                $senderEmail = empty($this->dataHelper->getGeneralConfig('sender_email') ? 'support@gloopro.com' : $this->dataHelper->getGeneralConfig('sender_email'));

                $sender = ['email' => $senderEmail, 'name' => 'septa'];
                
                $this->directory = $this->fileSystem->getDirectoryWrite(DirectoryList::VAR_DIR);

                $name = 'septa-products';
                $filepath = 'export/'. $name . '.csv';
                $this->directory->create('export');
                
                $stream = $this->directory->openFile($filepath, 'w+');
                $stream->lock();

                $columns = $this->productCollectionRepo->getSeptaProductCsvColumnHeader();
                foreach ($columns as $column) {
                    $header[] = $column;
                }
            
                $stream->writeCsv($header);

                $productCollection = $this->productCollectionRepo->all()->addInventoryCatalog()->get();
                $sizeOfProduct = $productCollection->getSize();

                $progressBar = new ProgressBar($output,  $sizeOfProduct );
                $progressBar->start();

                foreach ($productCollection as $product){
                    $productData = [];
                    $productData[] = $product->getSku();
                    $productData[] = null;
                    $productData[] = $product->getName();
                    $productData[] = $product->getName();
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = null;
                    $productData[] = $product->getQty();
                    $productData[] = $product->getCost();
                    $productData[] = $product->getPrice();

                    $stream->writeCsv($productData);

                    $progressBar->advance();
                }

                $progressBar->finish();
                echo "\n";

                $output->writeln('<info>Sending generated product csv to '.$recipeintEmail.'</info>');

                $csvContent = fopen(DirectoryList::VAR_DIR.'/'.$filepath, 'r');

                $transport = $this->transportBuilder
                ->setTemplateIdentifier('send_email_email_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setFrom($sender)
                ->addTo($recipeintEmail)
                ->setTemplateVars([])
                ->addAttachment($csvContent,'septa_bulk_product_upload.csv')
                ->getTransport();
                $transport->sendMessage();
                $output->writeln('<info>Done generating csv</info>');
                unlink(DirectoryList::VAR_DIR.'/'.$filepath);
            }catch(\Exception $e){
                $output->write($e->getMessage());
            }
        }
    }